package TestClassPackage;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import CommonMethodPackage.Trigger_patchapi;
import CommonUtilityPackage.HandleLogs;
import io.restassured.path.json.JsonPath;

public class Patch_tc extends Trigger_patchapi {
@Test	
	public static void executor() throws IOException {
		String requestBody = Patch_req();
		File dirname = HandleLogs.create_log_dir("Patch_tc1");
		int statuscode=0;
		for(int i=0; i<5; i++) {
			statuscode=extract_statuscode_Patch(Patch_req(), patch_endpoint());
				//System.out.println("Statucs code: " + statuscode);
				if (statuscode==200) {
					String ResponseBody=Trigger_patchapi.extract_body(Patch_req(), patch_endpoint());
					System.out.println("ResponseBody :"+ResponseBody);
				HandleLogs.evidence_creator(dirname, "Patch_tc1", get_endpoint(), Patch_req(), ResponseBody);
					validator(requestBody,ResponseBody);
					break;}
				else {
					System.out.println("Desired Code is not found");
				}
					
				}
		}
	public static void validator(String requestBody, String ResponseBody)throws IOException {
		
		JsonPath jsp_res = new JsonPath(ResponseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_updatedAt = jsp_res.getString("updatedAt");
		System.out.println(res_updatedAt);
		String mydate = res_updatedAt.substring(0, 10);

		// creating for request Body
		JsonPath jsp_req = new JsonPath(Patch_req());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime CurrentDate = LocalDateTime.now();
		String ExpectedDate = CurrentDate.toString().substring(0, 10);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(ExpectedDate, mydate);
		
		
		
	}
}
			
		
		
		

