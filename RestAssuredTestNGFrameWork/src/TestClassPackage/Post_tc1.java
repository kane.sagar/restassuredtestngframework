package TestClassPackage;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import org.testng.Assert;
import org.testng.annotations.Test;


//import static io.restassured.RestAssured.given;

import CommonMethodPackage.Triggerapimethod;
import CommonUtilityPackage.HandleLogs;
import RequestRepository.Post_requestrepository;
import io.restassured.path.json.JsonPath;

public class Post_tc1 extends Triggerapimethod {
	@Test
	public static void executor() throws IOException {
		String requestBody = post_tc1_request();
		File dirname = HandleLogs.create_log_dir("Post_tc1");
		for (int i = 0; i < 5; i++) {
			int statuscode = extract_statuscode_Post(requestBody, post_endpoint());
			// System.out.println("statuscode:" + statuscode);
			if (statuscode == 201) {
				String ResponseBody = extract_body(requestBody, post_endpoint());
				// System.out.println("ResponseBody:" + ResponseBody);
			     HandleLogs.evidence_creator(dirname, "Post_tc1", post_endpoint(),post_tc1_request(),ResponseBody);
				// post_tc1_request(), ResponseBody);

				validator(requestBody, ResponseBody);
				break;
			} else {
				System.out.println("Desired Status code is not found  please retry");
			}
		}
	}

//create the obj of Json path
	public static void validator(String requestBody, String ResponseBody) throws IOException {
		JsonPath jsp = new JsonPath(ResponseBody);
		String res_name = jsp.getString("name");
		String res_job = jsp.getString("job");
		int res_id = jsp.getInt("id");
		String res_createdAt = jsp.getString("createdAt");
		String mydate = res_createdAt.substring(0, 10);

		// request BODY
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime CurrentDate = LocalDateTime.now();
		String ExpectedDate = CurrentDate.toString().substring(0, 10);

		// validation
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(ExpectedDate, mydate);
		Assert.assertNotNull(res_id);
	}
}
