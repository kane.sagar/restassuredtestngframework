package TestClassPackage;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import CommonMethodPackage.Trigger_getapi;
import CommonUtilityPackage.HandleLogs;
import io.restassured.path.json.JsonPath;

public class Get_tc extends Trigger_getapi {
@Test
	public static void executor() throws IOException {
		File dirname = HandleLogs.create_log_dir("Get_tc1");
		for (int i = 0; i < 5; i++) {
			int statuscode = Trigger_getapi.extract_statuscode_Get(get_endpoint());
			System.out.println("statuscode is:" + statuscode);
			if (statuscode == 200) {
				String ResponseBody = Trigger_getapi.extract_response(get_endpoint());
				System.out.println("ResponseBody:" + ResponseBody);

				HandleLogs.evidence_creator(dirname, "Get_tc1", get_endpoint(), null, ResponseBody);

				validator(ResponseBody);
				break;
			} else {
				System.out.println("your code is invalid hence RETRY");
			}
		}
	}

	public static void validator(String ResponseBody) {
		String[] exp_id_array = { "7", "8", "9", "10", "11", "12" };
		String[] exp_email_array = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		String[] exp_firstname_array = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };

		JsonPath jsp_res = new JsonPath(ResponseBody);
		List<Object> res_data = jsp_res.getList("data");
		int count = res_data.size();
		for (int i = 0; i < count; i++) {
			String exp_id = exp_id_array[i];
			String res_id = jsp_res.getString("data[" + i + "].id");
			Assert.assertEquals(res_id, exp_id);
			// array of email
			String exp_email = exp_firstname_array[i];
			String res_email = jsp_res.getString("data[" + i + "].id");
			Assert.assertEquals(exp_email, exp_email);
			// array of first name
			String exp_firstname = exp_firstname_array[i];
			String res_firstname = jsp_res.getString("data[" + i + "].id");
			System.out.println("res_firstname" + res_firstname);
			System.out.println("res_id" + res_id);
			System.out.println("res_email" + res_email);
			Assert.assertEquals(exp_firstname, exp_firstname);
		}
	}
}
