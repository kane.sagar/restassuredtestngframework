package CommonMethodPackage;

import static io.restassured.RestAssured.given;

import RequestRepository.Endpoints;

public class Trigger_getapi extends Endpoints {
	public static int extract_statuscode_Get(String URL) {
		int statuscode = given().when().get(URL).then().extract().statusCode();
		return statuscode;
	}

	public static String extract_response(String URL) {

		String ResponseBody = given().when().get(URL).then().extract().response().asString();
		return ResponseBody;

	}

}
