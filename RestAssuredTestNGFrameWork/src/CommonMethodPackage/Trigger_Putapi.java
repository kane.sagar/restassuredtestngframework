package CommonMethodPackage;

import static io.restassured.RestAssured.given;

import RequestRepository.Put_requestBody;

public class Trigger_Putapi extends Put_requestBody {
	public static int extract_statuscode_Put(String req_body, String URL) {
		int statuscode = given().header("Content-Type", "application/json").body(req_body).when().put(URL).then()
				.extract().statusCode();
		return statuscode;

	}

	public static String extract_body(String req_body, String URL) {
		String ResponseBody = given().header("Content-Type", "application/json").body(req_body).when().put(URL).then()
				.extract().asString();
		return ResponseBody;

	}

}
