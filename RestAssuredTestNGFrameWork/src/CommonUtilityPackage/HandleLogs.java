package CommonUtilityPackage;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class HandleLogs {
	public static File create_log_dir(String dirname) {
		String currenprojectdir = System.getProperty("user.dir");
		System.out.println(currenprojectdir);
		File log_directory = new File(currenprojectdir + "//Api_Logs//" + dirname);
		delete_dir(log_directory);
		log_directory.mkdir();
		return  log_directory;
	}

	public static boolean delete_dir(File Directory) {
		boolean Directory_deleted = Directory.delete();
		if (Directory.exists()) {

			File[] files = Directory.listFiles();
			if (files != null) {
				for (File file : files) {
					if (file.isDirectory()) {
						delete_dir(file);
					} else {
						file.delete();
					}
				}
			}
			Directory_deleted = Directory.delete();

		}
		return Directory_deleted;
	}

	public static void evidence_creator(File dirname, String filename, String endpoint, String requestBody,
			String responseBody) throws IOException {
		// step1 create the file at given location
		File newfile = new File(dirname + "\\" + filename + ".txt");
		System.out.println("new file created to save evidence :" + newfile.getName());

		// step 2 write data into the file
		FileWriter datawriter = new FileWriter(newfile);
		datawriter.write("End point : " + endpoint + "\n\n");
		datawriter.write("Request Body : \n\n" + requestBody + "\n\n");
		datawriter.write("Response Body: \n\n" + responseBody);
		datawriter.close();
		System.out.println("EVIDENCE IS WRITTEN IN FILE : " + newfile.getName());

	}

}
